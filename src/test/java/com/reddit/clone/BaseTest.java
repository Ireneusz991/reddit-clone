package com.reddit.clone;

import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;

public class BaseTest {
    @Container
   static MySQLContainer mySQLContainer = (MySQLContainer) new MySQLContainer("mysql:latest")
            .withDatabaseName("reddit-clone")
            .withUsername("root")
            .withPassword("root")
            .withReuse(true);

    static {
        mySQLContainer.start();
    }

}
