package com.reddit.clone.service;

import com.reddit.clone.exceptions.SpringRedditException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class CommentServiceTest {

    @Test
    @DisplayName("Test should pass when comment don't contains swear words")
    void shouldNotContainSwearWordsInComments() {
        CommentService commentService = new CommentService(null,null,null,null,null, null,null);
        assertThat(commentService.containsSwearWords("This is clean comment")).isFalse();

    }

    @Test
    @DisplayName("Should Throw Exception when Exception Contains Swear Words")
    void shouldFailWhenCommentContainsSwearWords() {
        CommentService commentService = new CommentService(null, null, null, null, null, null, null);

        assertThatThrownBy(() -> {
            commentService.containsSwearWords("This is a shitty comment");
        }).isInstanceOf(SpringRedditException.class)
                .hasMessage("Comments contains unacceptable language");
    }
}